﻿using BioStarCSharp;
using SupremaAccessControl.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SupremaAccessControl.Services
{
    public class CardService
    {
        BSSDK.BSMifareConfig bsMifareConfig;
        BSSDK.BEUserHdr beUserHdr;

        private long ConvertNFC2SupremaCardID(string hexStr)
        {
            string HxInvert = "";
            long decNum = 0;
            try
            {
                foreach (var str in splitString(hexStr, 2))
                {
                    HxInvert = str + HxInvert;
                }
                decNum = Convert.ToInt64(HxInvert, 16);
                return decNum;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private static uint userID = 1;
        public bool EnrollUser(string cardID, DeviceConfig device)
        {
            long card = this.ConvertNFC2SupremaCardID(cardID);
            beUserHdr.version = 1;
            beUserHdr.userID = userID++;
            beUserHdr.startTime = 0;
            beUserHdr.expiryTime = 365 * 24 * 60 * 60;
            beUserHdr.adminLevel = 0;
            beUserHdr.securityLevel = 0;
            beUserHdr.accessGroupMask = (uint)Convert.ToInt64("FFFFFFFF", 16);
            beUserHdr.opMode = BSSDK.BS_AUTH_MODE_DISABLED;
            beUserHdr.cardVersion = (byte)Convert.ToInt64("13", 16); ;
            beUserHdr.cardFlag = (byte)'\0';
            beUserHdr.cardID = (uint)card;
            beUserHdr.cardCustomID = (byte)0;

            try
            {
                int enrollSize = Marshal.SizeOf(beUserHdr);
                IntPtr enrollPtr = Marshal.AllocHGlobal(enrollSize);

                Marshal.StructureToPtr(beUserHdr, enrollPtr, true);


                int res = BSSDK.BS_EnrollUserBEPlus(device.TcpHandle, enrollPtr, null);
                if (res == BSSDK.BS_SUCCESS)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool GetAllUsers(DeviceConfig device)
        {
            int numOfUsers = 0;
            int numOfTemp = 0;
            try
            {
                int result = BSSDK.BS_GetUserDBInfo(device.TcpHandle, ref numOfUsers, ref numOfTemp);
                if (result != BSSDK.BS_SUCCESS)
                {

                    return false;
                }


                BSSDK.BEUserHdr[] userHdr = new BSSDK.BEUserHdr[numOfUsers];
                IntPtr userInfo = Marshal.AllocHGlobal(numOfUsers * Marshal.SizeOf(typeof(BSSDK.BEUserHdr)));

                result = BSSDK.BS_GetAllUserInfoBEPlus(device.TcpHandle, userInfo, ref numOfUsers);

                if (result != BSSDK.BS_SUCCESS && result != BSSDK.BS_ERR_NOT_FOUND)
                {
                    Marshal.FreeHGlobal(userInfo);
                    return false;
                }

                for (int i = 0; i < numOfUsers; i++)
                {
                    userHdr[i] = (BSSDK.BEUserHdr)Marshal.PtrToStructure(new IntPtr(userInfo.ToInt32() + i * Marshal.SizeOf(typeof(BSSDK.BEUserHdr))), typeof(BSSDK.BEUserHdr));

                    Debug.WriteLine(userHdr[i].userID.ToString());
                    Debug.WriteLine(userHdr[i].cardID.ToString());

                }

                Marshal.FreeHGlobal(userInfo);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool DeleteAllUsers(DeviceConfig device)
        {
            try
            {
                int res = BSSDK.BS_DeleteAllUser(device.TcpHandle);
                return res == BSSDK.BS_SUCCESS ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public CardService()
        {
        }
        private IEnumerable<string> splitString(string str, int chunkSize)
        {
            if (String.IsNullOrEmpty(str) || chunkSize < 1 || str.Length % chunkSize != 0)
                throw new Exception("Invalid string or chunksize");
            return Enumerable.Range(0, str.Length / chunkSize).Select(i => str.Substring(i * chunkSize, chunkSize));
        }
        public string readData(DeviceConfig device)
        {

            uint cardID = 0;
            int custID = 0;
            int res = BSSDK.BS_ReadCardIDEx(device.TcpHandle, ref cardID, ref custID);
            if (res != BSSDK.BS_SUCCESS)
            {
                return res == -102 ? res.ToString() : "";
            }
            string Hx = Convert.ToString(cardID, 16).ToUpper();
            string HxInvert = "";
            try
            {
                foreach (var str in splitString(Hx, 2))
                {
                    HxInvert = str + HxInvert;
                }


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return "";
            }


            if (res == 0)
            {
                var str = HxInvert;
                return str;
            }
            else return "";
        }

        public void writeMifareConfigData(DeviceConfig device)
        {
            bsMifareConfig.bioentryCompatible = 0;
            bsMifareConfig.useCSNOnly = 1;
            bsMifareConfig.cisIndex = 4;
            bsMifareConfig.disabled = 0;
            bsMifareConfig.useSecondaryKey = 0;
            bsMifareConfig.numOfTemplate = 2;
            bsMifareConfig.templateSize = 334;
            bsMifareConfig.templateStartBlock = new int[] { 8, 36, -1, -1 };

            int configSize = Marshal.SizeOf(bsMifareConfig);
            IntPtr config = Marshal.AllocHGlobal(configSize);

            Marshal.StructureToPtr(bsMifareConfig, config, true);


            int res = BSSDK.BS_WriteMifareConfiguration(device.TcpHandle, config);
            bsMifareConfig = (BSSDK.BSMifareConfig)Marshal.PtrToStructure(config, typeof(BSSDK.BSMifareConfig));

            Marshal.FreeHGlobal(config);

        }
        public void readMifareConfigData(DeviceConfig device)
        {
            IntPtr mifareInfo = Marshal.AllocHGlobal(Marshal.SizeOf(bsMifareConfig));

            int res = BSSDK.BS_ReadMifareConfiguration(device.TcpHandle, mifareInfo);
            bsMifareConfig = (BSSDK.BSMifareConfig)Marshal.PtrToStructure(mifareInfo, typeof(BSSDK.BSMifareConfig));

            Marshal.FreeHGlobal(mifareInfo);
        }

    }
}
