﻿using SupremaAccessControl.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SupremaAccessControl.Services
{
    public class DeviceSerializationService
    {
        private XmlSerializer serializer = null;
        private static string fileName = "config_list.xml";

        private bool XmlFileExists()
        {
            return File.Exists(fileName);
        }

        private bool DeviceExists(uint DeviceID)
        {
            if (XmlFileExists())
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                var v = (List<DeviceConfig>)serializer.Deserialize(fs);
                fs.Close();

                return v.Exists(x => x.DeviceID == DeviceID);
            }
            return false;
        }

        public DeviceSerializationService()
        {
            this.serializer = new XmlSerializer(typeof(List<DeviceConfig>));
        }

        public List<DeviceConfig> ReadDeviceList()
        {
            if (XmlFileExists())
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                var v = (List<DeviceConfig>)serializer.Deserialize(fs);
                fs.Close();

                return v;
            }
            else
            {
                return null;
            }
        }

        public DeviceConfig GetDevice(uint DeviceID)
        {
            if (XmlFileExists())
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                var v = (List<DeviceConfig>)serializer.Deserialize(fs);
                fs.Close();

                return v.FirstOrDefault(x => x.DeviceID == DeviceID);
            }
            return null;
        }

        public bool AddOrUpdateDevice(DeviceConfig device)
        {
            List<DeviceConfig> devices = this.ReadDeviceList();
            if (devices == null)
            {
                devices = new List<DeviceConfig>();
                devices.Add(device);
            }
            else if (devices.Exists(x => x.DeviceID == device.DeviceID))
            {
                DeviceConfig v = devices.First(x => x.DeviceID == device.DeviceID);
                int idx = devices.IndexOf(v);
                devices[idx] = device;
            }
            else
            {
                devices.Add(device);
            }

            FileStream fs = new FileStream(fileName, FileMode.Create);
            serializer.Serialize(fs, devices);
            fs.Close();
            return true;
        }
    }
}
