﻿using BioStarCSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SupremaAccessControl.Services
{
    class SdkSetupService
    {
        public bool LoadBiostarSdkAsync()
        {
            try
            {
                var res = BSSDK.BS_InitSDK();
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Counld not initiate SDK successfully");
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public bool UnloadBiostarSdkAsync()
        {
            try
            {
                var res = BSSDK.BS_UnInitSDK();
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Counld not Close SDK successfully");
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
