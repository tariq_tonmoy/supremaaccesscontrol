﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace SupremaAccessControl.Services
{
    public class DbConn
    {
        public static string ConnStr = "Server=10.220.20.6; database=smartiut; port=3306; UID=smart; password=secret;";
        public MySqlConnection Connection { get; set; }
        public bool IsConnected { get; set; }

        public DbConn()
        {
            this.IsConnected = false;
        }
        public void SetupConnection()
        {
            if (Connection == null || !this.IsConnected)
            {
                try
                {
                    Connection = new MySqlConnection(ConnStr);
                    Connection.Open();
                    this.IsConnected = true;
                }
                catch (Exception)
                {

                    this.IsConnected = false;
                }
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (Connection != null || IsConnected)
                {
                    Connection.Close();
                    IsConnected = false;

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
