﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using SupremaAccessControl.Model;
using System.Threading;

namespace SupremaAccessControl.Services
{
    public class DatabaseService
    {

        DbConn conn = new DbConn();
        public delegate void GetAttendanceDetailsDelegate(string cardID, string studentID, string roomID, int? trainsactionID, DateTime dateTime);
        public event GetAttendanceDetailsDelegate OnAttendanceSuccess;

        public delegate void DBConnErrorDelegate(string errorString);
        public event DBConnErrorDelegate OnDBError;

        public DatabaseService(MainWindow window, DbConn conn)
        {
            window.OnSetAttendance += Window_OnSetAttendance;
            try
            {
                //conn.SetupConnection();
                this.conn = conn;
            }
            catch (Exception)
            {


            }

        }

        private void Window_OnSetAttendance(string cardID, DateTime dateTime, string roomNo)
        {
            Task<string> getIDTask = GetStudentID(conn, cardID);
            if (getIDTask != null)
            {
                if (getIDTask.Result != null)
                {
                    getIDTask.ContinueWith((resID) =>
                    {
                        Task<int?> getPresenceTask = GetPresenceInRoom(conn, resID.Result, roomNo, dateTime);
                        if (getPresenceTask != null)
                        {
                            getPresenceTask.ContinueWith((resPresence) =>
                            {
                                Task<bool> setAttendanceTask = AddAttendanceTransaction(conn, resID.Result, roomNo, resPresence.Result, dateTime);
                                if (setAttendanceTask != null)
                                    setAttendanceTask.ContinueWith((result) =>
                                    {
                                        if (this.OnAttendanceSuccess != null)
                                            this.OnAttendanceSuccess(cardID, resID.Result, roomNo, resPresence.Result, dateTime);
                                    });
                            });
                        }
                    });
                }
            }
        }

        public Task<string> GetStudentID(DbConn conn, string cardID)
        {
            //return Task.Run(() => { return CommonVariables.studentList[cardID]; });

            if (conn.Connection == null)
            {
                return Task.Run(() => { return (string)null; });
            }

            string sql = "SELECT student_id FROM student_position WHERE card_id=\'" + cardID + "\'";
            return Task.Run(() =>
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(sql, conn.Connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    string str = "";
                    while (reader.Read())
                    {
                        str = reader[0] as string;
                        reader.Close();
                        break;
                    }
                    return str;
                }
                catch (Exception ex)
                {
                    if (this.OnDBError != null)
                        OnDBError(ex.Message);
                    return null;
                }
            });
        }



        public Task<int?> GetPresenceInRoom(DbConn conn, string studentID, string roomID, DateTime date)
        {
            //return Task.Run(() =>
            //{
            //    return (int?)CommonVariables.studentList.Values.ToList().IndexOf(studentID);
            //});



            if (conn.Connection == null)
            {
                return Task.Run(() => { return (int?)null; });
            }

            string sql = "SELECT t_id FROM trans_attendance WHERE student_id='" + studentID + "' AND room_id='" + roomID + "' AND date='" + date.ToString("yyyy-MM-dd") + "' AND exit_time IS NULL";
            return Task.Run(() =>
            {
                try
                {

                    MySqlCommand cmd = new MySqlCommand(sql, conn.Connection);
                    var v = cmd.ExecuteScalar();
                    return ((int?)v);
                }
                catch (Exception ex)
                {
                    if (this.OnDBError != null)
                        OnDBError(ex.Message);
                    return (int?)null;
                }
            });
        }





        public Task<bool> AddAttendanceTransaction(DbConn conn, string studentID, string roomID, int? t_id, DateTime dateTime)
        {
            //return Task.Run(() =>
            //{
            //    Debug.WriteLine(studentID + " " + roomID + " " + t_id.ToString() + " " + dateTime.ToString());
            //    return true;
            //});

            if (conn.Connection == null)
            {
                return Task.Run(() => { return false; });
            }


            string query = "";
            if (t_id != null)
            {
                query = "UPDATE trans_attendance SET exit_time='" + dateTime.ToString("HH:mm:ss") + "' WHERE t_id=" + t_id.ToString(); ;
            }
            else
            {
                query = "INSERT INTO trans_attendance (student_id, room_id, date, entry_time) VALUES ('" + studentID + "','" + roomID + "','" + dateTime.ToString("yyyy-MM-dd") + "','" + dateTime.ToString("HH:mm:ss") + "')";
            }
            return Task.Run(() =>
            {
                try
                {

                    Debug.WriteLine(studentID + " " + roomID + " " + t_id.ToString() + " " + dateTime.ToString());

                    MySqlCommand cmd = new MySqlCommand(query, conn.Connection);
                    var v = cmd.ExecuteNonQuery();
                    return true;
                } 
                catch (Exception ex)
                {
                    if (this.OnDBError != null)
                        OnDBError(ex.Message);
                    return false;
                }
            });
        }

    }


}
