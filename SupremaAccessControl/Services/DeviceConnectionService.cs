﻿using BioStarCSharp;
using SupremaAccessControl.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SupremaAccessControl.Services
{
    public class DeviceConnectionService
    {
        BSSDK.BEConfigData m_ConfigBEPlus;
        public DeviceConnectionService()
        {
        }

        public DeviceConfig ConnectToDevice(DeviceConfig device)
        {
            try
            {
                int tcpHandle = 0;
                var res = BSSDK.BS_OpenSocket(device.DeviceAddrString, CommonVariables.PORT, ref tcpHandle);
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Cannot connect to TCP");
                device.TcpHandle = tcpHandle;
                res = BSSDK.BS_SetDeviceID(device.TcpHandle, device.DeviceID, device.DeviceType);
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Cannot Set Device ID");
                device.IsConnected = true;
                return device;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public uint? CloseConnection(DeviceConfig device)
        {
           
            try
            {
                var res = BSSDK.BS_CloseSocket(device.TcpHandle);
                if (res == BSSDK.BS_SUCCESS)
                {
                    return device.DeviceID;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private BSSDK.BEConfigData GetConfigData(DeviceConfig device)
        {
            int configSize = 0;

            IntPtr config = Marshal.AllocHGlobal(Marshal.SizeOf(m_ConfigBEPlus));

            int result = BSSDK.BS_ReadConfigUDP(device.UdpHandle, device.DeviceAddr, device.DeviceID, BSSDK.BEPLUS_CONFIG, ref configSize, config);


            if (result != BSSDK.BS_SUCCESS)
            {

                Marshal.FreeHGlobal(config);
                return new BSSDK.BEConfigData();
            }

            m_ConfigBEPlus = (BSSDK.BEConfigData)Marshal.PtrToStructure(config, typeof(BSSDK.BEConfigData));

            Marshal.FreeHGlobal(config);
            return m_ConfigBEPlus;
        }
        public DeviceConfig ReadConnectionConfig(DeviceConfig device)
        {
            if (device.DeviceType == BSSDK.BS_DEVICE_XPASS_SLIM2)
            {
                try
                {
                    m_ConfigBEPlus = this.GetConfigData(device);
                    if (m_ConfigBEPlus.ipAddr == (uint)0)
                    {
                        return null;
                    }
                    IPAddress addr = null;
                    uint m_DeviceAddr = m_ConfigBEPlus.ipAddr;

                    addr = new IPAddress(m_ConfigBEPlus.ipAddr);
                    device.DeviceAddrString = addr.ToString();
                    if (device.DeviceAddrString != addr.ToString())
                        return null;

                    addr = new IPAddress(m_ConfigBEPlus.gateway);
                    device.Gateway = addr.ToString();

                    addr.Address = m_ConfigBEPlus.subnetMask;
                    device.SubnetMask = addr.ToString();


                    device.Port = m_ConfigBEPlus.port;
                    return device;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else return null;
        }

        public bool WriteConnectionConfig(DeviceConfig device)
        {
            try
            {
                var tempDevice = new DeviceConfig() { DeviceID = device.DeviceID, UdpHandle = device.UdpHandle, DeviceAddr = device.DeviceAddr };
                tempDevice.DeviceAddr = device.ConstDeviceAddr;
                m_ConfigBEPlus = this.GetConfigData(tempDevice);
                string addrStr = "";
                uint m_DeviceAddr = m_ConfigBEPlus.ipAddr;

                IPAddress addr = IPAddress.Parse(device.DeviceAddrString);
                m_ConfigBEPlus.ipAddr = (uint)addr.Address;

                addr = IPAddress.Parse(device.Gateway);
                m_ConfigBEPlus.gateway = (uint)addr.Address;

                addr = IPAddress.Parse(device.SubnetMask);
                m_ConfigBEPlus.subnetMask = (uint)addr.Address;
                m_ConfigBEPlus.useDHCP = false;

                m_ConfigBEPlus.useServer = false;
                m_ConfigBEPlus.synchTime = false;


                m_ConfigBEPlus.serverIpAddr = (uint)0;

                m_ConfigBEPlus.port = device.Port;

                m_ConfigBEPlus.ledBuzzerConfig.buzzerPattern[42].repeat = 1;
                m_ConfigBEPlus.ledBuzzerConfig.buzzerPattern[43].repeat = -1;

                int configSize = Marshal.SizeOf(m_ConfigBEPlus);
                IntPtr config = Marshal.AllocHGlobal(configSize);

                Marshal.StructureToPtr(m_ConfigBEPlus, config, true);

                int result = BSSDK.BS_WriteConfigUDP(device.UdpHandle, device.ConstDeviceAddr, device.DeviceID, BSSDK.BEPLUS_CONFIG, configSize, config);

                Marshal.FreeHGlobal(config);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
