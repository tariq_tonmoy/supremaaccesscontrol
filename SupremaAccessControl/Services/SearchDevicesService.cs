﻿using BioStarCSharp;
using SupremaAccessControl.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupremaAccessControl.Services
{
    public class SearchDevicesService
    {
        private int _Handle = -1;
        private int _NumOfDevices;
        private uint[] _DeviceIDs;
        private int[] _DeviceTypes;
        private uint[] _DeviceAddrs;

        public SearchDevicesService()
        {
            _DeviceIDs = new uint[CommonVariables.MAX_DEVICE];
            _DeviceTypes = new int[CommonVariables.MAX_DEVICE];
            _DeviceAddrs = new uint[CommonVariables.MAX_DEVICE];
        }
        public List<DeviceConfig> SearchDevicesAsync()
        {
            List<DeviceConfig> devices = new List<DeviceConfig>();

            try
            {

                var res = BSSDK.BS_OpenInternalUDP(ref _Handle);
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Opening UDP Failed");

                res = BSSDK.BS_SearchDeviceInLAN(_Handle, ref _NumOfDevices, _DeviceIDs, _DeviceTypes, _DeviceAddrs);
                if (res != BSSDK.BS_SUCCESS)
                    throw new Exception("Searching in LAN Failed");

                for (int i = 0; i < _NumOfDevices; i++)
                {
                    devices.Add(new DeviceConfig()
                    {
                        DeviceID = _DeviceIDs[i],
                        DeviceAddr = _DeviceAddrs[i],
                        DeviceType = _DeviceTypes[i],
                        UdpHandle = _Handle
                    });
                }
                DeviceSerializationService serializationService = new DeviceSerializationService();
                foreach(var v in devices)
                {
                    var temp = serializationService.GetDevice(v.DeviceID);
                    v.RoomNo = temp == null ? "" : temp.RoomNo;
                }
                return devices;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
