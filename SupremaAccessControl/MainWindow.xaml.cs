﻿using SupremaAccessControl.Model;
using SupremaAccessControl.Services;
using SupremaAccessControl.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SupremaAccessControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
        HomeViewModel viewModel = new HomeViewModel();
        SdkSetupService setupService = new SdkSetupService();
        SearchDevicesService search = new SearchDevicesService();
        DeviceConnectionService deviceConnect = new DeviceConnectionService();
        DeviceSerializationService serialization = new DeviceSerializationService();
        DbConn conn = new DbConn();
        CardService card = new CardService();

        public delegate void SetAttendanceDelegate(string cardID, DateTime dateTime, string roomNo);
        public event SetAttendanceDelegate OnSetAttendance;

        private async void InitializaSDK()
        {
            var v = await Task.Run(() => setupService.LoadBiostarSdkAsync());
            viewModel.IsSdkInitialized = v;
          

        }

        private void DbService_OnAttendanceSuccess(string cardID, string studentID, string roomID, int? trainsactionID, DateTime dateTime)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                viewModel.CardInfo += "Card ID: " + cardID + "; Student ID: " + studentID + "; Room ID: " + roomID + "; Transaction ID: " + trainsactionID.ToString() + "; Time: " + dateTime.ToString() + "\n";
            }));
        }
        private void  showDeviceInfo(string deviceID, string roomID, DateTime dateTime, string message)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                viewModel.CardInfo += "Device ID: " + deviceID+ "; Room ID: " + roomID + "; Time: " + dateTime.ToString() + " Message: "+ message+"\n";
            }));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            InitializaSDK();
            viewModel.IsSdkInitialized = false;
            viewModel.IsSearchDone = true;
            viewModel.ConnectionConfigButtonString = "Edit";
            viewModel.IsConnectEnabled = true;
            viewModel.IpAddrTxtVisibility = Visibility.Collapsed;
            viewModel.SubnetMaskTxtVisibility = Visibility.Collapsed;
            viewModel.SubnetMaskPanelVisibility = Visibility.Collapsed;
            viewModel.RoomNoTextVisibility = Visibility.Collapsed;
            viewModel.GatewayPanelVisibility = Visibility.Collapsed;

            viewModel.SearchButtonVisibility = Visibility.Visible;
            viewModel.CloseAllButtonVisibility = Visibility.Collapsed;

            viewModel.IsSearchEnabled = true;
            viewModel.IsCloseAllEnabled = true;
            viewModel.IsDeviceListEnabled = true;

            this.MakeConnectButtonVisible();
            this.MakeEditButtonVisible();

            viewModel.Devices = new ObservableCollection<Model.DeviceConfig>();

            this.DataContext = viewModel;
            try
            {
                conn.SetupConnection();
                var dbService = new DatabaseService(this, conn);
                dbService.OnAttendanceSuccess += DbService_OnAttendanceSuccess;
                dbService.OnDBError += DbService_OnDBError;
            }catch(Exception ex)
            {
                DbService_OnDBError(ex.Message);
            }
        }

        private void DbService_OnDBError(string errorString)
        {
            Debug.WriteLine(errorString);
            conn.CloseConnection();
        }

        private void MakeConnectButtonVisible()
        {
            viewModel.ConnectButtonVisibility = Visibility.Visible;
            viewModel.SaveButtonVisibility = Visibility.Collapsed;
            viewModel.DisconnectButtonVisibility = Visibility.Collapsed;
        }
        private void MakeSaveButtonVisible()
        {
            viewModel.ConnectButtonVisibility = Visibility.Collapsed;
            viewModel.SaveButtonVisibility = Visibility.Visible;
            viewModel.DisconnectButtonVisibility = Visibility.Collapsed;
        }
        private void MakeDisconnectButtonVisible()
        {
            viewModel.ConnectButtonVisibility = Visibility.Collapsed;
            viewModel.SaveButtonVisibility = Visibility.Collapsed;
            viewModel.DisconnectButtonVisibility = Visibility.Visible;
        }
        private void MakeEditButtonVisible()
        {
            viewModel.EditButtonVisibility = Visibility.Visible;
            viewModel.DiscardButtonVisibility = Visibility.Collapsed;
            viewModel.ReadCardButtonVisibility = Visibility.Collapsed;
            viewModel.StopReadButtonVisibility = Visibility.Collapsed;
        }
        private void MakeReadButtonVisible()
        {
            viewModel.EditButtonVisibility = Visibility.Collapsed;
            viewModel.DiscardButtonVisibility = Visibility.Collapsed;
            viewModel.ReadCardButtonVisibility = Visibility.Visible;
            viewModel.StopReadButtonVisibility = Visibility.Collapsed;
        }
        private void MakeDiscardButtonVisible()
        {
            viewModel.EditButtonVisibility = Visibility.Collapsed;
            viewModel.DiscardButtonVisibility = Visibility.Visible;
            viewModel.ReadCardButtonVisibility = Visibility.Collapsed;
            viewModel.StopReadButtonVisibility = Visibility.Collapsed;
        }
        private void MakeStopReadButtonVisible()
        {
            viewModel.EditButtonVisibility = Visibility.Collapsed;
            viewModel.DiscardButtonVisibility = Visibility.Collapsed;
            viewModel.ReadCardButtonVisibility = Visibility.Collapsed;
            viewModel.StopReadButtonVisibility = Visibility.Visible;
        }


        private Task<uint?> CloseConnection(DeviceConfig device)
        {
            if (device != null)
            {
                device.IsReading = false;
                if (device.IsConnected)
                {
                    return Task.Run(() => deviceConnect.CloseConnection(device));
                }
            }
            return Task.Run(() => { return (uint?)0; });
        }
        private void close_all_button_click(object sender, RoutedEventArgs e)
        {
            List<Task<uint?>> closeConnTasks = new List<Task<uint?>>();
            foreach (var v in viewModel.Devices)
            {
                closeConnTasks.Add(this.CloseConnection(v));
            }

            foreach (var v in closeConnTasks)
            {
                v.ContinueWith((res) =>
                {
                    var deviceID = res.Result;
                    if (deviceID > 0)
                    {
                        var device = (from d in viewModel.Devices
                                      where d.DeviceID == deviceID
                                      select d).ToList().FirstOrDefault();
                        if (device != null)
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                this.viewModel.Devices.Remove(device);
                            }));
                        }

                    }

                    Debug.WriteLine(res.Result);
                });
            }

            var devices = (from d in viewModel.Devices
                           where !d.IsConnected
                           select d).ToList();

            for (int i = 0; i < devices.Count; i++)
            {
                this.viewModel.Devices.Remove(devices[i]);
            }
            viewModel.SearchButtonVisibility = Visibility.Visible;
            viewModel.CloseAllButtonVisibility = Visibility.Collapsed;

        }

        private async void Button_Click_search(object sender, RoutedEventArgs e)
        {

            viewModel.IsSearchEnabled = false;
            var devices = await Task.Run(() => search.SearchDevicesAsync());
            foreach (var v in devices)
            {
                viewModel.Devices.Add(v);
            }
            viewModel.IsSearchEnabled = true;
            if (devices.Count > 0)
            {
                viewModel.SearchButtonVisibility = Visibility.Collapsed;
                viewModel.CloseAllButtonVisibility = Visibility.Visible;
                viewModel.IsCloseAllEnabled = true;
            }
        }


       

        private void StopReadButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            v.IsReading = false;
            this.MakeDisconnectButtonVisible();
            this.MakeReadButtonVisible();
        }

        private async void ReadCardButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            if (v.IsConnected)
            {
                if (v.IsReading)
                    v.IsReading = false;
                else v.IsReading = true;

                this.MakeDisconnectButtonVisible();
                this.MakeStopReadButtonVisible();

                while (v.IsReading)
                {
                    v.IsReading = true;
                    var str = await Task.Run(() => card.readData(v));

                    if (!String.IsNullOrEmpty(str))
                    {
                        if (str == "-102")
                        {
                            Console.WriteLine("in main.xaml: " + str);
                            v.IsConnected = false;
                            v.IsReading = true;
                            DeviceConfig tempConfig = v;
                            try
                            {
                                v= await Task.Run(() => deviceConnect.ConnectToDevice(v));
                                if (v == null)
                                    throw new Exception();
                                this.showDeviceInfo(tempConfig.DeviceID.ToString(), tempConfig.RoomNo, DateTime.Now.Date, "Reconnected");
                                Console.WriteLine("Reconnected");
                            }
                            catch (Exception)
                            {
                                v = tempConfig;
                                this.showDeviceInfo(tempConfig.DeviceID.ToString(),tempConfig.RoomNo,DateTime.Now.Date, "Failed to Reconnect");

                            }
                        }
                        else
                        {
                            if (this.OnSetAttendance != null)
                                this.OnSetAttendance(str, DateTime.Now, v.RoomNo);
                        }
                    }
                }
            }
            else return;


        }

        private async void DiscardButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            viewModel.SelectedDevice = null;
            v = await Task.Run(() => serialization.GetDevice(v.DeviceID));
            viewModel.SelectedDevice = v;

            var temp = viewModel.Devices.First(x => x.DeviceID == v.DeviceID);
            int idx = viewModel.Devices.IndexOf(temp);
            viewModel.Devices[idx] = v;

            viewModel.SubnetMaskTxtVisibility = Visibility.Collapsed;
            viewModel.IpAddrTxtVisibility = Visibility.Collapsed;
            viewModel.SubnetMaskPanelVisibility = Visibility.Collapsed;
            viewModel.RoomNoTextVisibility = Visibility.Collapsed;
            viewModel.GatewayTxtVisibility = Visibility.Collapsed;
            viewModel.GatewayPanelVisibility = Visibility.Collapsed;

            viewModel.IsDeviceListEnabled = true;

            this.MakeConnectButtonVisible();
            this.MakeEditButtonVisible();
        }

        private async void EditButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            viewModel.IsDiscardButtonEnabled = true;
            v.ConstDeviceAddr = viewModel.SelectedDevice.DeviceAddr;
            viewModel.SelectedDevice = null;
            v = await Task.Run(() => deviceConnect.ReadConnectionConfig(v));
            if (v != null)
            {
                try
                {
                    v.RoomNo = (await Task.Run(() => serialization.GetDevice(v.DeviceID))).RoomNo;
                }catch(Exception ex) { }
                viewModel.SelectedDevice = v;

                viewModel.SubnetMaskTxtVisibility = Visibility.Visible;
                viewModel.IpAddrTxtVisibility = Visibility.Visible;
                viewModel.SubnetMaskPanelVisibility = Visibility.Visible;
                viewModel.GatewayPanelVisibility = Visibility.Visible;
                viewModel.RoomNoTextVisibility = Visibility.Visible;
                viewModel.GatewayTxtVisibility = Visibility.Visible;
                viewModel.IsDeviceListEnabled = false;
                this.MakeSaveButtonVisible();
                this.MakeDiscardButtonVisible();
                viewModel.IsDeviceDetailsEnabled = true;
            }
            else
                MessageBox.Show("Error Reading Data from Device", "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
        }

        private async void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            viewModel.SelectedDevice = null;
            var res = this.CloseConnection(v);
            await res.ContinueWith((reply) =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    this.MakeConnectButtonVisible();
                    this.MakeEditButtonVisible();
                    v.IsConnected = false;
                    viewModel.SelectedDevice = v;
                }));
            });

        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null) return;
            ((Button)sender).IsEnabled = false;
            viewModel.IsDiscardButtonEnabled = false;
            bool deviceRes = await Task.Run(() => deviceConnect.WriteConnectionConfig(v));
            bool serializeRes = await Task.Run(() => serialization.AddOrUpdateDevice(v));

            if (serializeRes && deviceRes)
            {
                v.ConstDeviceAddr = viewModel.SelectedDevice.DeviceAddr;
                viewModel.SelectedDevice = v;
                viewModel.SubnetMaskTxtVisibility = Visibility.Collapsed;
                viewModel.IpAddrTxtVisibility = Visibility.Collapsed;
                viewModel.SubnetMaskPanelVisibility = Visibility.Collapsed;
                viewModel.GatewayPanelVisibility = Visibility.Collapsed;
                viewModel.RoomNoTextVisibility = Visibility.Collapsed;
                viewModel.GatewayTxtVisibility = Visibility.Collapsed;

                this.MakeConnectButtonVisible();
                this.MakeEditButtonVisible();
            }
            else
            {
                viewModel.SelectedDevice = serialization.GetDevice(v.DeviceID);
                viewModel.SubnetMaskTxtVisibility = Visibility.Collapsed;
                viewModel.IpAddrTxtVisibility = Visibility.Collapsed;
                viewModel.SubnetMaskPanelVisibility = Visibility.Collapsed;
                viewModel.GatewayPanelVisibility = Visibility.Collapsed;
                viewModel.RoomNoTextVisibility = Visibility.Collapsed;
                viewModel.GatewayTxtVisibility = Visibility.Collapsed;

                MessageBox.Show("Error while Saving", "Error", MessageBoxButton.OK, MessageBoxImage.Stop);

                viewModel.SelectedDevice = null;
                v = await Task.Run(() => serialization.GetDevice(v.DeviceID));
                viewModel.SelectedDevice = v;

                var temp = viewModel.Devices.First(x => x.DeviceID == v.DeviceID);
                int idx = viewModel.Devices.IndexOf(temp);
                viewModel.Devices[idx] = v;
            }
            viewModel.IsDeviceListEnabled = true;
            viewModel.IsDiscardButtonEnabled = true;
            ((Button)sender).IsEnabled = true;
        }

        private async void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            var v = viewModel.SelectedDevice;
            if (v == null)
            {
                return;
            }
            viewModel.SelectedDevice = null;
            if (!v.IsConnected)
                v = await Task.Run(() => deviceConnect.ConnectToDevice(v));
            if (v != null)
            {
                viewModel.SelectedDevice = v;
                this.MakeDisconnectButtonVisible();
                this.MakeReadButtonVisible();
            }
        }



        private void Device_ListBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                viewModel.IsDeviceDetailsEnabled = true;
                if (viewModel.SelectedDevice.IsConnected)
                {
                    if (viewModel.SelectedDevice.IsReading)
                    {
                        MakeDisconnectButtonVisible();
                        MakeStopReadButtonVisible();
                    }
                    else
                    {
                        MakeDisconnectButtonVisible();
                        MakeReadButtonVisible();
                    }
                }
                else
                {
                    MakeConnectButtonVisible();
                    MakeEditButtonVisible();
                }
            }
            else
            {
                viewModel.IsDeviceDetailsEnabled = false;

            }
        }
    }
}
