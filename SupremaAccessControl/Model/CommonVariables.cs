﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupremaAccessControl.Model
{
    public class CommonVariables
    {
        public const int MAX_DEVICE = 128;
        public const int PORT = 1471;
        public static Dictionary<string, string> studentList = new Dictionary<string, string>()
        {
            {"3ED20A46","11111111A" },
            {"0B5DB123","11111111B" },
            {"EB59B123","11111111C" },
            {"3E09EE45","11111111D" },
            {"2B60B123","11111111E" },
            {"AEDD5DF0","160041058" },
            {"2EDF78F0","160041042" },
            {"2E450A46","160021076" },
            {"FEB60C46","160021051" },
            {"FED60746","160021027" }
        };
    }
}
