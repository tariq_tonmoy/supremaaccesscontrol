﻿using BioStarCSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupremaAccessControl.Model
{
    public class DeviceConfig
    {
        private string _DeviceTypeString;
        private int _DeviceType;
        private uint _DeviceAddr;
        private string _DeviceAddrString;
        private uint _ConstDeviceAddr;
        private bool _IsReading;

        public uint DeviceID { get; set; }
        public int UdpHandle { get; set; }
        public int TcpHandle { get; set; }
        public string Gateway { get; set; }
        public string SubnetMask { get; set; }
        public int Port { get; set; }
        public string RoomNo { get; set; }

        public bool IsConnected { get; set; }
        public string DeviceTypeString { get => _DeviceTypeString; }
        public int DeviceType { get => _DeviceType; set => SetDeviceType(value); }
        public uint DeviceAddr { get => _DeviceAddr; set => SetDeviceAddr(value); }
        public string DeviceAddrString { get => _DeviceAddrString; set => SetDeviceAddrString(value); }
        public uint ConstDeviceAddr { get => _ConstDeviceAddr; set => _ConstDeviceAddr = value; }
        public bool IsReading { get => _IsReading; set => _IsReading = value; }

        private void SetDeviceType(int value)
        {
            _DeviceType = value;
            string device = "";
            if (_DeviceType == BSSDK.BS_DEVICE_BIOSTATION)
            {
                device += "BioStation ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_DSTATION)
            {
                device += "D-Station ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_XSTATION)
            {
                device += "X-Station ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_BIOSTATION2)
            {
                device += "BioStation T2 ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_FSTATION)
            {
                device += "FaceStation ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_BIOENTRY_PLUS)
            {
                device += "BioEntry Plus ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_BIOENTRY_W)
            {
                device += "BioEntry W ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_BIOLITE)
            {
                device += "BioLite Net ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_XPASS)
            {
                device += "Xpass ";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_XPASS_SLIM)
            {
                device += "Xpass Slim";
            }
            else if (_DeviceType == BSSDK.BS_DEVICE_XPASS_SLIM2)
            {
                device += "Xpass S2";
            }
            else
            {
                device += "Unknown ";
            }
            _DeviceTypeString = device;
        }
        private void SetDeviceAddr(uint value)
        {
            this._DeviceAddr = value;
            this._DeviceAddrString = "";
            this._DeviceAddrString += (this._DeviceAddr & 0xff) + ".";
            this._DeviceAddrString += ((this._DeviceAddr >> 8) & 0xff) + ".";
            this._DeviceAddrString += ((this._DeviceAddr >> 16) & 0xff) + ".";
            this._DeviceAddrString += ((this._DeviceAddr >> 24) & 0xff);
        }

        private void SetDeviceAddrString(string addrStr)
        {
            this._DeviceAddrString = addrStr;
            this._DeviceAddr = (uint)IPAddress.Parse(addrStr).Address;
        }

    }
}
