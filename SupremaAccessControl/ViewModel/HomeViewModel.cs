﻿using SupremaAccessControl.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SupremaAccessControl.ViewModel
{
    public class HomeViewModel : NotifyPropertyChangedCommon
    {
        private bool _IsSdkInitialized, _IsSearchDone;
        private ObservableCollection<DeviceConfig> _Devices;
        private DeviceConfig _SelectedDevice;
        private bool _IsConnectEnabled;
        private string _ConnectionConfigButtonString;
        private Visibility _IpAddrTxtVisibility, _SubnetMaskTxtVisibility;
        private Visibility _SearchButtonVisibility, _ConnectButtonVisibility, _CloseAllButtonVisibility, _DisconnectButtonVisibility, _EditButtonVisibility, _SaveButtonVisibility, _DiscardButtonVisibility, _ReadCardButtonVisibility, _StopReadButtonVisibility;
        private Visibility _SubnetMaskPanelVisibility;
        private Visibility _RoomNoTextVisibility;
        private bool _IsSearchEnabled, _IsCloseAllEnabled;
        private bool _IsDiscardButtonEnabled;
        private bool _IsDeviceDetailsEnabled;
        private string _CardInfo;
        private string _UserToEnroll;
        private bool _IsDeviceListEnabled;
        private Visibility _gatewayTxtVisibility;
        private Visibility _gatewayPanelVisibility;

        public ObservableCollection<DeviceConfig> Devices { get => _Devices; set => SetProperty(ref this._Devices, value); }
        public bool IsSdkInitialized { get => _IsSdkInitialized; set => SetProperty(ref this._IsSdkInitialized, value); }
        public DeviceConfig SelectedDevice { get => _SelectedDevice; set => SetProperty(ref this._SelectedDevice, value); }

        public bool IsSearchDone { get => _IsSearchDone; set => SetProperty(ref this._IsSearchDone, value); }
        public string ConnectionConfigButtonString { get => _ConnectionConfigButtonString; set => SetProperty(ref this._ConnectionConfigButtonString, value); }
        public Visibility IpAddrTxtVisibility { get => _IpAddrTxtVisibility; set => SetProperty(ref this._IpAddrTxtVisibility, value); }
        public Visibility SubnetMaskTxtVisibility { get => _SubnetMaskTxtVisibility; set => SetProperty(ref this._SubnetMaskTxtVisibility, value); }
        public string CardInfo { get => _CardInfo; set => SetProperty(ref this._CardInfo, value); }
        public string UserToEnroll { get => _UserToEnroll; set => SetProperty(ref this._UserToEnroll, value); }
        public bool IsConnectEnabled { get => _IsConnectEnabled; set => SetProperty(ref _IsConnectEnabled, value); }


        public Visibility SearchButtonVisibility { get => _SearchButtonVisibility; set => SetProperty(ref _SearchButtonVisibility, value); }
        public Visibility ConnectButtonVisibility { get => _ConnectButtonVisibility; set => SetProperty(ref _ConnectButtonVisibility, value); }
        public Visibility CloseAllButtonVisibility { get => _CloseAllButtonVisibility; set => SetProperty(ref _CloseAllButtonVisibility, value); }
        public Visibility DisconnectButtonVisibility { get => _DisconnectButtonVisibility; set => SetProperty(ref _DisconnectButtonVisibility, value); }
        public Visibility EditButtonVisibility { get => _EditButtonVisibility; set => SetProperty(ref _EditButtonVisibility, value); }
        public Visibility SaveButtonVisibility { get => _SaveButtonVisibility; set => SetProperty(ref _SaveButtonVisibility, value); }
        public Visibility DiscardButtonVisibility { get => _DiscardButtonVisibility; set => SetProperty(ref _DiscardButtonVisibility, value); }
        public Visibility ReadCardButtonVisibility { get => _ReadCardButtonVisibility; set => SetProperty(ref _ReadCardButtonVisibility, value); }
        public Visibility StopReadButtonVisibility { get => _StopReadButtonVisibility; set => SetProperty(ref _StopReadButtonVisibility, value); }
        public bool IsSearchEnabled { get => _IsSearchEnabled; set => SetProperty(ref _IsSearchEnabled, value); }
        public bool IsCloseAllEnabled { get => _IsCloseAllEnabled; set => SetProperty(ref _IsCloseAllEnabled, value); }
        public Visibility SubnetMaskPanelVisibility { get => _SubnetMaskPanelVisibility; set => SetProperty(ref _SubnetMaskPanelVisibility, value); }
        public Visibility RoomNoTextVisibility { get => _RoomNoTextVisibility; set => SetProperty(ref _RoomNoTextVisibility, value); }
        public bool IsDeviceDetailsEnabled { get => _IsDeviceDetailsEnabled; set => SetProperty(ref _IsDeviceDetailsEnabled, value); }
        public bool IsDeviceListEnabled { get => _IsDeviceListEnabled; set => SetProperty(ref _IsDeviceListEnabled, value); }
        public bool IsDiscardButtonEnabled { get => _IsDiscardButtonEnabled; set =>SetProperty(ref _IsDiscardButtonEnabled , value); }
        public Visibility GatewayTxtVisibility { get => _gatewayTxtVisibility; set =>SetProperty(ref _gatewayTxtVisibility , value); }
        public Visibility GatewayPanelVisibility { get => _gatewayPanelVisibility; set =>SetProperty(ref _gatewayPanelVisibility , value); }
    }

}
